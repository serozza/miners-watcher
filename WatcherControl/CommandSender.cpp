#include "CommandSender.h"
#include <QTcpSocket>

CommandSender::CommandSender(const QString& watcherAddr, const int port, QObject *parent)
    : QObject(parent)
    , m_watcherAddr(watcherAddr)
    , m_watcherPort(port)
    , m_pTcpSocket(new QTcpSocket(this))
    , m_wasUsed(false)
{
    connect(m_pTcpSocket, &QTcpSocket::connected, this, &CommandSender::slotConnected);
}

void CommandSender::sendReboot(const int gpioNum)
{
    m_currentCommand = QString("REBOOT:%1").arg(gpioNum);
    connectToWatcher();
}

void CommandSender::sendRebootAll()
{
    m_currentCommand = "REBOOTALL";
    connectToWatcher();
}

void CommandSender::sendTurnOnOff(const int gpioNum)
{
    m_currentCommand = QString("TURNONOFF:%1").arg(gpioNum);
    connectToWatcher();
}

void CommandSender::sendTurnOnOffAll()
{
    m_currentCommand = "TURNONOFFALL";
    connectToWatcher();
}

void CommandSender::sendHardOff(const int gpioNum)
{
    m_currentCommand = QString("HARDOFF:%1").arg(gpioNum);
    connectToWatcher();
}

void CommandSender::sendHardOffAll()
{
    m_currentCommand = "HARDOFFALL";
    connectToWatcher();
}

void CommandSender::sendManualCommand(const QString &command)
{
    m_currentCommand = command;
    connectToWatcher();
}

void CommandSender::slotConnected()
{
    sendCommand();
}

void CommandSender::connectToWatcher()
{
    if(!m_wasUsed)
    {
        m_pTcpSocket->connectToHost(m_watcherAddr, m_watcherPort);
        m_wasUsed = true;
    }
}

void CommandSender::sendCommand()
{
    QByteArray data(m_currentCommand.toLocal8Bit());
    m_pTcpSocket->write(data);
    m_pTcpSocket->close();
}
