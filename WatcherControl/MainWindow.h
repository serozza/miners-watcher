#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class CommandSender;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_watcherIpEdit_textChanged(const QString &arg1);
    void on_portEdit_textChanged(const QString &arg1);
    void on_rebootButton_clicked();
    void on_rebootAllButton_clicked();
    void on_sendCommandButton_clicked();
    void on_turnOnOffButton_clicked();
    void on_turnOnOffAllButton_clicked();
    void on_hardOffButton_clicked();
    void on_hardOffAllButton_clicked();

private:
    void init();
    CommandSender *createCommandSender();
    int getSelectedRigNum();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
