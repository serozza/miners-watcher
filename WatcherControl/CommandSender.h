#ifndef COMMANDSENDER_H
#define COMMANDSENDER_H

#include <QObject>

class QTcpSocket;

class CommandSender : public QObject
{
    Q_OBJECT
public:
    explicit CommandSender(const QString& watcherAddr, const int port, QObject *parent = nullptr);
    void sendReboot(const int gpioNum);
    void sendRebootAll();
    void sendTurnOnOff(const int gpioNum);
    void sendTurnOnOffAll();
    void sendHardOff(const int gpioNum);
    void sendHardOffAll();
    void sendManualCommand(const QString& command);

private slots:
    void slotConnected();

private:
    void connectToWatcher();
    void sendCommand();

private:
    const QString m_watcherAddr;
    const int m_watcherPort;
    QTcpSocket* m_pTcpSocket;
    QString m_currentCommand;
    bool m_wasUsed;
};

#endif // COMMANDSENDER_H
