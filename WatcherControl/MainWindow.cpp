#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QSettings>
#include "CommandSender.h"
#include <QTimer>

static const QString k_userDataFile = "user_data.conf";
static const QString k_lastWatcherIpKey = "LastWatcherIp";
static const QString k_lastWatcherPortKey = "LastWatcherPort";
static const int k_commandSenderLifeTime = 30000;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    const int gpioFirstPin = 1;
    const int gpioLastPin = 26;

    for (unsigned int i = gpioFirstPin; i <= gpioLastPin; ++i)
    {
        ui->gpioNumCombo->addItem(QString::number(i));
    }

    QSettings settings(k_userDataFile, QSettings::IniFormat);
    QString lastWatcherIp = settings.value(k_lastWatcherIpKey, QVariant("")).toString();
    ui->watcherIpEdit->setText(lastWatcherIp);

    QString watcherPort = settings.value(k_lastWatcherPortKey, QVariant("5123")).toString();
    ui->portEdit->setText(watcherPort);
    ui->portEdit->setValidator( new QIntValidator(1, 65535, this) );
}

CommandSender* MainWindow::createCommandSender()
{
    CommandSender* commandSender = new CommandSender(ui->watcherIpEdit->text(), ui->portEdit->text().toInt());
    QTimer::singleShot(k_commandSenderLifeTime, [commandSender]{ delete commandSender; });
    return commandSender;
}

int MainWindow::getSelectedRigNum()
{
    return ui->gpioNumCombo->currentText().toInt();
}

void MainWindow::on_watcherIpEdit_textChanged(const QString &arg1)
{
    QSettings settings(k_userDataFile, QSettings::IniFormat);
    settings.setValue(k_lastWatcherIpKey, arg1);
}

void MainWindow::on_portEdit_textChanged(const QString &arg1)
{
    QSettings settings(k_userDataFile, QSettings::IniFormat);
    settings.setValue(k_lastWatcherPortKey, arg1);
}

void MainWindow::on_rebootButton_clicked()
{
    const int rigNum = getSelectedRigNum();
    createCommandSender()->sendReboot(rigNum);
}

void MainWindow::on_rebootAllButton_clicked()
{
    createCommandSender()->sendRebootAll();
}

void MainWindow::on_sendCommandButton_clicked()
{
    createCommandSender()->sendManualCommand(ui->manualCommandEdit->text());
}

void MainWindow::on_turnOnOffButton_clicked()
{
    const int rigNum = getSelectedRigNum();
    createCommandSender()->sendTurnOnOff(rigNum);
}

void MainWindow::on_turnOnOffAllButton_clicked()
{
    createCommandSender()->sendTurnOnOffAll();
}

void MainWindow::on_hardOffButton_clicked()
{
    const int rigNum = getSelectedRigNum();
    createCommandSender()->sendHardOff(rigNum);
}

void MainWindow::on_hardOffAllButton_clicked()
{
    createCommandSender()->sendHardOffAll();
}
