#ifndef WATCHER_H
#define WATCHER_H

#include <memory>
#include <thread>
#include "Network/ICommandHandler.h"
#include "GPIO/GPIOTypes.h"

namespace GPIO {
class IGPIO;
}

namespace Network {
class ITCPServer;
}

namespace TasksQ {
class SimpleTaskQ;
}

class Watcher : public Network::ICommandHandlerObserver
{
public:
    Watcher();
    ~Watcher();

    int run(const int port, bool invertedMode = false);
    // ICommandHandlerObserver interface
    void commandReceived(const Network::Command command);

private:
    std::unique_ptr<Network::ITCPServer> m_tcpServer;
    std::unique_ptr<Network::ICommandHandler> m_commandHandler;
    std::unique_ptr<TasksQ::SimpleTaskQ> m_taskQueue;
};

#endif // WATCHER_H
