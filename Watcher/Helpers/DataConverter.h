#ifndef DATACONVERTER_H
#define DATACONVERTER_H

#include "GPIO/GPIODevice.h"
#include "Watcher.h"
#include "Network/NetworkTypes.h"

namespace Helpers {

class DataConverter
{
public:
    static std::string toString(const Network::eCommandTypes commandType);
    static GPIO::ePins toGPIO(const int rigNumber);

private:
    DataConverter() = default;
};

}

#endif // DATACONVERTER_H
