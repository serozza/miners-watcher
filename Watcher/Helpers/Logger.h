#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <iostream>

namespace Helpers {

class Logger
{
public:
    static void toLog(std::string logStr);
    static std::ostream& log();

private:
    Logger() = default;
};

}

#endif // LOGGER_H
