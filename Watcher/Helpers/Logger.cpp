#include "Logger.h"
#include <iostream>
#include <stdio.h>

void Helpers::Logger::toLog(std::__cxx11::string logStr)
{
    std::cout << logStr << std::endl;
}

std::ostream& Helpers::Logger::log()
{
    std::cout << std::endl;
    return std::cout;
}
