#include "DataConverter.h"

std::string Helpers::DataConverter::toString(const Network::eCommandTypes commandType)
{
    return Network::toString(commandType);
}

GPIO::ePins Helpers::DataConverter::toGPIO(const int rigNumber)
{
    return static_cast<GPIO::ePins>(rigNumber + 3);
}


