#include "Watcher.h"
#include "Helpers/DataConverter.h"
#include "Helpers/Logger.h"
#include "GPIO/RemoteRigAPI.h"
#include "Network/TCPServer.h"
#include "Network/CommandHandler.h"
#include "TasksQ/SimpleTaskQ.h"

Watcher::Watcher()
    : m_tcpServer(nullptr)
    , m_commandHandler(new Network::CommandHandler())
    , m_taskQueue(new TasksQ::SimpleTaskQ())
{
    boost::asio::io_service io_service;
    m_tcpServer.reset(new Network::TCPServer(io_service));

    m_tcpServer->attachObserver(m_commandHandler.get());
    m_commandHandler->attachObserver(this);
}

Watcher::~Watcher()
{
}

int Watcher::run(const int port, bool invertedMode)
{
    if(invertedMode)
        GPIO::RemoteRigAPI::enableInvertedMode();
    else
        GPIO::RemoteRigAPI::disableInvertedMode();

    if(m_taskQueue)
        m_taskQueue->run();

    if(m_tcpServer)
        m_tcpServer->run(port);

    return 0;
}

void Watcher::commandReceived(const Network::Command command)
{
    Helpers::Logger::log() << "command type: "  << Network::toString(command.type()) << std::endl
                           << "command value: " << command.value();

    Task task = []{};
    switch (command.type())
    {
    case Network::eCommandTypes::Reboot:
    {
        GPIO::ePins pin = Helpers::DataConverter::toGPIO(std::stoi(command.value()));
        task = [=]{ GPIO::RemoteRigAPI::resetDevice(pin); };
        break;
    }
    case Network::eCommandTypes::TurnOnOff:
    {
        GPIO::ePins pin = Helpers::DataConverter::toGPIO(std::stoi(command.value()));
        task = [=]{ GPIO::RemoteRigAPI::turnOnOffDevice(pin); };
        break;
    }
    case Network::eCommandTypes::HardOff:
    {
        GPIO::ePins pin = Helpers::DataConverter::toGPIO(std::stoi(command.value()));
        task = [=]{ GPIO::RemoteRigAPI::hardTurnOffDevice(pin); };
        break;
    }
    case Network::eCommandTypes::RebootAll:
    {
        task = [=]{ GPIO::RemoteRigAPI::resetAllDevices(); };
        break;
    }
    case Network::eCommandTypes::TurnOnOffAll:
    {
        task = [=]{ GPIO::RemoteRigAPI::turnOnOffAllDevices(); };
        break;
    }
    case Network::eCommandTypes::HardOffAll:
    {
        task = [=]{ GPIO::RemoteRigAPI::hardTurnOffAllDevices(); };
        break;
    }
    default: return;
    }

    m_taskQueue->push(task);
}
