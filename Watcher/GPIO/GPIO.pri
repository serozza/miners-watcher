HEADERS += \
    $$PWD/GPIODevice.h \
    $$PWD/GPIOTypes.h \
    $$PWD/IGPIO.h \
    $$PWD/RemoteRigAPI.h \
    $$PWD/Util.h

SOURCES += \
    $$PWD/GPIODevice.cpp \
    $$PWD/RemoteRigAPI.cpp \
    $$PWD/Util.cpp
