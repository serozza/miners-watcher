#ifndef GPIOUTIL_H
#define GPIOUTIL_H

#include "IGPIO.h"

namespace GPIO {

class Util
{
    Util() = delete;

public:
    static void blink(const int pin);
};

}

#endif // GPIOUTIL_H
