#ifndef REMOTERIGAPI_H
#define REMOTERIGAPI_H

#include "IGPIO.h"

namespace GPIO {

class RemoteRigAPI
{
    RemoteRigAPI() = delete;
public:
    static void enableInvertedMode();
    static void disableInvertedMode();
    static void resetDevice(const GPIO::ePins pin);
    static void turnOnOffDevice(const GPIO::ePins pin);
    static void hardTurnOffDevice(const GPIO::ePins pin);
    static void resetAllDevices();
    static void turnOnOffAllDevices();
    static void hardTurnOffAllDevices();
};
}
#endif // REMOTERIGAPI_H
