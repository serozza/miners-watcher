#ifndef GPIOTYPES_H
#define GPIOTYPES_H

namespace GPIO {

enum ePins
{
    PIN_GPIO_2 = 2,
    PIN_GPIO_3,
    PIN_GPIO_4,
    PIN_GPIO_5,
    PIN_GPIO_6,
    PIN_GPIO_7,
    PIN_GPIO_8,
    PIN_GPIO_9,
    PIN_GPIO_10,
    PIN_GPIO_11,
    PIN_GPIO_12,
    PIN_GPIO_13,
    PIN_GPIO_14,
    PIN_GPIO_15,
    PIN_GPIO_16,
    PIN_GPIO_17,
    PIN_GPIO_18,
    PIN_GPIO_19,
    PIN_GPIO_20,
    PIN_GPIO_21,
    PIN_GPIO_22,
    PIN_GPIO_23,
    PIN_GPIO_24,
    PIN_GPIO_25,
    PIN_GPIO_26,
    PIN_GPIO_27
};

}
#endif // GPIOTYPES_H
