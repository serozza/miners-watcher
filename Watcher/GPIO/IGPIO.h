#ifndef IGPIO_H
#define IGPIO_H

#include "GPIOTypes.h"

namespace GPIO {

class IGPIO
{
public:
    virtual ~IGPIO(){}
    virtual void sendLogicOne(ePins pin) = 0;
    virtual void sendLogicZero(ePins pin) = 0;
    virtual void sendLogicOneForeach() = 0;
    virtual void sendLogicZeroForeach() = 0;
    virtual void systemDelay(unsigned int millis) = 0;
    virtual void enableInvertedMode() = 0;
    virtual void disableInvertedMode() = 0;
};

class ILogicPointsHandler
{
public:
    virtual ~ILogicPointsHandler(){}
    virtual int logicOne() = 0;
    virtual int logicZero() = 0;
};

}
#endif // IGPIO_H
