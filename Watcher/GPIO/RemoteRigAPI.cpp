#include "RemoteRigAPI.h"
#include "GPIODevice.h"
#include "Helpers/Logger.h"

using namespace GPIO;

static const unsigned int k_sysTimeout_1sec = 1000;
static const unsigned int k_sysTimeout_2sec = 2000;
static const unsigned int k_sysTimeout_5sec = 5000;

void RemoteRigAPI::enableInvertedMode()
{
    GPIO::GPIODevice::instance(true).enableInvertedMode();
}

void RemoteRigAPI::disableInvertedMode()
{
    GPIO::GPIODevice::instance().disableInvertedMode();
}

void GPIO::RemoteRigAPI::resetDevice(const GPIO::ePins pin)
{
    Helpers::Logger::log() << "resetDevice" << std::endl;
    hardTurnOffDevice(pin);
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_2sec);
    turnOnOffDevice(pin);
}

void GPIO::RemoteRigAPI::turnOnOffDevice(const GPIO::ePins pin)
{
    Helpers::Logger::log() << "turnOnOffDevice" << std::endl;
    GPIO::GPIODevice::instance().sendLogicOne(pin);
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_1sec);
    GPIO::GPIODevice::instance().sendLogicZero(pin);
}

void GPIO::RemoteRigAPI::hardTurnOffDevice(const GPIO::ePins pin)
{
    Helpers::Logger::log() << "hardTurnOffDevice" << std::endl;
    GPIO::GPIODevice::instance().sendLogicOne(pin);
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_5sec);
    GPIO::GPIODevice::instance().sendLogicZero(pin);
}

void GPIO::RemoteRigAPI::resetAllDevices()
{
    Helpers::Logger::log() << "resetAllDevices" << std::endl;
    GPIO::GPIODevice::instance().sendLogicOneForeach();
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_5sec);

    GPIO::GPIODevice::instance().sendLogicZeroForeach();
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_2sec);

    GPIO::GPIODevice::instance().sendLogicOneForeach();
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_1sec);

    GPIO::GPIODevice::instance().sendLogicZeroForeach();
}

void GPIO::RemoteRigAPI::turnOnOffAllDevices()
{
    Helpers::Logger::log() << "turnOnOffAllDevices" << std::endl;
    GPIO::GPIODevice::instance().sendLogicOneForeach();
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_1sec);
    GPIO::GPIODevice::instance().sendLogicZeroForeach();
}

void GPIO::RemoteRigAPI::hardTurnOffAllDevices()
{
    Helpers::Logger::log() << "hardTurnOffAllDevices" << std::endl;
    GPIO::GPIODevice::instance().sendLogicOneForeach();
    GPIO::GPIODevice::instance().systemDelay(k_sysTimeout_5sec);
    GPIO::GPIODevice::instance().sendLogicZeroForeach();
}
