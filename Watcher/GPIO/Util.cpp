#include "Util.h"
#include "GPIODevice.h"

using namespace GPIO;

void Util::blink(const int pin)
{
    const unsigned int delayBetweenBliks = 500;

    for(int i = 0; i < 5; ++i)
    {
        GPIO::GPIODevice::instance().sendLogicOne(static_cast<GPIO::ePins>(pin));
        GPIO::GPIODevice::instance().systemDelay(delayBetweenBliks);

        GPIO::GPIODevice::instance().sendLogicZero(static_cast<GPIO::ePins>(pin));
        GPIO::GPIODevice::instance().systemDelay(delayBetweenBliks);
    }
}
