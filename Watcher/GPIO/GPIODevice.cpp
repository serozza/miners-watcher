#include "GPIODevice.h"
#include <iostream>
#include "bcm2835/bcm2835.h"

class LogicPointsHandler : public GPIO::ILogicPointsHandler
{
    // ILogicPointsHandler interface
private:
    int logicOne() { return HIGH; }
    int logicZero(){ return LOW; }
};

class InvertedLogicPointsHandler : public GPIO::ILogicPointsHandler
{
    // ILogicPointsHandler interface
private:
    int logicOne() { return LOW; }
    int logicZero(){ return HIGH; }
};


GPIO::GPIODevice::GPIODevice(bool invertedStart)
    : m_logicPointsHandler(invertedStart
                           ? static_cast<GPIO::ILogicPointsHandler*>(new InvertedLogicPointsHandler)
                           : static_cast<GPIO::ILogicPointsHandler*>(new LogicPointsHandler))
{
    initBcm2835();
    prepareAllPins();
}

GPIO::GPIODevice::~GPIODevice()
{
    int errCode = bcm2835_close();
    if(errCode != 1)
    {
        std::cout << "bcm2835 lib was not closed correctly! ( code: " << errCode << " )" << std::endl;
    }
}

GPIO::GPIODevice& GPIO::GPIODevice::instance(bool invertedStart)
{
    static GPIODevice instObj(invertedStart);
    return instObj;
}

void GPIO::GPIODevice::sendLogicOne(GPIO::ePins pin)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    sendLogicOneNotThreadSave(pin);
}

void GPIO::GPIODevice::sendLogicZero(GPIO::ePins pin)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    sendLogicZeroNotThreadSave(pin);
}

void GPIO::GPIODevice::systemDelay(unsigned int millis)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    bcm2835_delay(millis);
}

void GPIO::GPIODevice::sendLogicOneForeach()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    for(int pin = GPIO::PIN_GPIO_2; pin <= GPIO::PIN_GPIO_27; ++pin)
    {
        sendLogicOneNotThreadSave(static_cast<GPIO::ePins>(pin));
    }
}

void GPIO::GPIODevice::sendLogicZeroForeach()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    for(int pin = GPIO::PIN_GPIO_2; pin <= GPIO::PIN_GPIO_27; ++pin)
    {
        sendLogicZeroNotThreadSave(static_cast<GPIO::ePins>(pin));
    }
}

void GPIO::GPIODevice::enableInvertedMode()
{
    m_logicPointsHandler.reset(new InvertedLogicPointsHandler);
}

void GPIO::GPIODevice::disableInvertedMode()
{
    m_logicPointsHandler.reset(new LogicPointsHandler);
}

void GPIO::GPIODevice::sendLogicOneNotThreadSave(GPIO::ePins pin)
{
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, m_logicPointsHandler->logicOne());
}

void GPIO::GPIODevice::sendLogicZeroNotThreadSave(GPIO::ePins pin)
{
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, m_logicPointsHandler->logicZero());
}

void GPIO::GPIODevice::initBcm2835()
{
    if (!bcm2835_init())
    {
        std::cout << "bcm2835 lib was not initialized!" << std::endl;
        std::exit(1);
    }
    else
    {
        std::cout << "bcm2835 initialized successfuly!" << std::endl;
    }
}

void GPIO::GPIODevice::prepareAllPins()
{
    sendLogicZeroForeach();
}
