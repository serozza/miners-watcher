#ifndef GPIODEVICE_H
#define GPIODEVICE_H

#include "IGPIO.h"
#include <mutex>
#include <memory>

namespace GPIO {

  class GPIODevice : public virtual IGPIO
  {
  public:
    virtual ~GPIODevice();
    static GPIODevice& instance(bool invertedStart = false);

    // IGPIO interface
  public:
    virtual void sendLogicOne(ePins pin) override;
    virtual void sendLogicZero(ePins pin) override;
    virtual void systemDelay(unsigned int millis) override;
    virtual void sendLogicOneForeach() override;
    virtual void sendLogicZeroForeach() override;
    virtual void enableInvertedMode() override;
    virtual void disableInvertedMode() override;

  private:
    GPIODevice(bool invertedStart = false);
    GPIODevice(GPIODevice& obj) = default;
    GPIODevice& operator=(GPIODevice& obj) = default;
    void initBcm2835();
    void prepareAllPins();
    void sendLogicOneNotThreadSave(ePins pin);
    void sendLogicZeroNotThreadSave(ePins pin);

    std::unique_ptr<ILogicPointsHandler> m_logicPointsHandler;
    std::mutex m_mutex;
  };
}

#endif // GPIODEVICE_H
