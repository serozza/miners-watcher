TEMPLATE = app
CONFIG += console c++11 thread
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CC = arm-linux-gnueabi-gcc
QMAKE_CXX = arm-linux-gnueabi-g++
QMAKE_LINK = arm-linux-gnueabi-g++

QMAKE_LFLAGS += -static-libstdc++ -static-libgcc

include(TasksQ/TasksQ.pri)
include(GPIO/GPIO.pri)
include(Helpers/Helpers.pri)
include(Network/Network.pri)
include(bcm2835/bcm2835.pri)
include(boost_info.pri)

SOURCES += main.cpp \
    Watcher.cpp

HEADERS += \
    Watcher.h
