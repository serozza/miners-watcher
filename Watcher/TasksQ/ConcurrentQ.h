#ifndef CONCURRENTQ_H
#define CONCURRENTQ_H

#include <mutex>
#include <queue>
#include <condition_variable>

namespace TasksQ {

template <typename T>
class ConcurrentQ
{
public:
    ConcurrentQ();
    void push(const T& obj);
    T pop();
    T waitAndPop();
    bool empty();

private:
    std::mutex mMutex;
    std::condition_variable mQReadyCondition;
    std::queue<T> mQueue;
};
}

#include "ConcurrentQ.tcc"
#endif // CONCURRENTQ_H
