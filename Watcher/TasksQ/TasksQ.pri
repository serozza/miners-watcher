INCLUDEPATH += .

HEADERS += \
    $$PWD/ITaskQ.h \
    $$PWD/SimpleTaskQ.h \
    $$PWD/ConcurrentQ.h

SOURCES += \
    $$PWD/SimpleTaskQ.cpp \
    $$PWD/ConcurrentQ.tcc
