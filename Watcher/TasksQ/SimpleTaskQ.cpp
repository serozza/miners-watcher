#include "SimpleTaskQ.h"
#include "Helpers/Logger.h"

using namespace TasksQ;

SimpleTaskQ::SimpleTaskQ()
{

}

SimpleTaskQ::~SimpleTaskQ()
{
    shutdown();

    if(mWorkerThead.joinable())
        mWorkerThead.join();
}

void SimpleTaskQ::shutdown()
{
    workDone = true;
}

void SimpleTaskQ::push(const Task &task)
{
    mTasksQ.push(task);
}

void SimpleTaskQ::run()
{
    Helpers::Logger::log() << "Start of task queue ... " << std::endl;
    auto workerFunc = [=](ConcurrentQ<TasksQ::Task>* q, const bool* const workDone)
    {
        while (true)
        {
            if(*workDone)
                break;

            try
            {
                auto task = q->waitAndPop();
                task();
                Helpers::Logger::log() << "Task done" << std::endl;
            }
            catch(std::out_of_range &e)
            {
                Helpers::Logger::log() << "Exception! " << e.what() << std::endl;
            }
        }
    };

    mWorkerThead = std::thread(workerFunc, &mTasksQ, &workDone);
}
