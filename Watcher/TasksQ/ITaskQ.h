#ifndef ITASKQ_H
#define ITASKQ_H

#include <functional>

namespace TasksQ {

using Task = std::function<void(void)>;
class ITaskQ
{
public:
    virtual ~ITaskQ(){};
    virtual void push(const Task& task) = 0;
    virtual void run() = 0;
    virtual void shutdown() = 0;
};

}
#endif // ITASKQ_H
