#include "ConcurrentQ.h"

using namespace TasksQ;

template <typename T>
ConcurrentQ<T>::ConcurrentQ()
{

}

template<typename T>
void ConcurrentQ<T>::push(const T &obj)
{
    std::lock_guard<std::mutex> lock(mMutex);
    mQueue.push(obj);
    mQReadyCondition.notify_one();
}

template<typename T>
T ConcurrentQ<T>::pop()
{
    std::lock_guard<std::mutex> lock(mMutex);

    if(mQueue.empty())
        throw std::out_of_range("Queue is empty!");

    T ret = mQueue.front();
    mQueue.pop();
    return ret;
}

template<typename T>
T ConcurrentQ<T>::waitAndPop()
{
    std::unique_lock<std::mutex> lock(mMutex);
    mQReadyCondition.wait(lock, [this]
    {
        return !mQueue.empty();
    });

    T ret = mQueue.front();
    mQueue.pop();
    return ret;
}

template<typename T>
bool ConcurrentQ<T>::empty()
{
    std::lock_guard<std::mutex> lock(mMutex);
    return mQueue.empty();
}
