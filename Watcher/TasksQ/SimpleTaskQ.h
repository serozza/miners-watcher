#ifndef SIMPLETASKQ_H
#define SIMPLETASKQ_H

#include <queue>
#include <thread>
#include "ITaskQ.h"
#include "ConcurrentQ.h"

namespace TasksQ {

class SimpleTaskQ : public ITaskQ
{
public:
    SimpleTaskQ();
    ~SimpleTaskQ();

    // ITaskQ interface
public:
    void push(const TasksQ::Task &task) override;
    void run() override;
    void shutdown() override;

private:
    ConcurrentQ<TasksQ::Task> mTasksQ;
    std::thread mWorkerThead;
    bool workDone = false;
};
}

#endif // SIMPLETASKQ_H
