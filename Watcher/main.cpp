#include <iostream>
#include "Watcher.h"
#include "GPIO/Util.h"
#include <string>

static const int DEFAULT_SERVER_PORT = 5123;

void showHelp()
{
    std::cout << "Help: " << std::endl
              << "-b [pin number], --blink [pin number]: just try to blink corresponding pin" << std::endl
              << "-r [port number], --run [port number]: run Watcher on corresponding port" << std::endl
              << "run it without any argument for running watcher on default " << DEFAULT_SERVER_PORT << " port" << std::endl;
}

int main(int argc, char* argv[])
{
    Watcher watcherApp;

    bool isAdditionalArgumentsPresent = (argc > 1);

    if(isAdditionalArgumentsPresent)
    {
        for(int i = 0; i < argc; ++i)
        {
            if(std::string(argv[i]) == std::string("-b") || std::string(argv[i]) == std::string("--blink"))
            {
                std::string pin = argv[i + 1];
                int pinNum = std::stoi(pin);
                GPIO::Util::blink(pinNum);
            }
            else if(std::string(argv[i]) == std::string("-r") || std::string(argv[i]) == std::string("--run"))
            {
                std::string port = argv[i + 1];
                int portNum = std::stoi(port);

                watcherApp.run(portNum);
            }
            else if(std::string(argv[i]) == std::string("-ir") || std::string(argv[i]) == std::string("--invertedrun"))
            {
                std::string port = argv[i + 1];
                int portNum = std::stoi(port);

                watcherApp.run(portNum, true);
            }
            else if(std::string(argv[i]) == std::string("-h") || std::string(argv[i]) == std::string("--help"))
            {
                showHelp();
            }
        }
    }

    return isAdditionalArgumentsPresent ? 0 : watcherApp.run(DEFAULT_SERVER_PORT);
}
