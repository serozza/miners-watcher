#include "TCPServer.h"
#include "Helpers/Logger.h"
#include <ctime>

Network::TCPServer::TCPServer(boost::asio::io_service& io_service)
{

}

std::__cxx11::string Network::TCPServer::getDateTime()
{
    using namespace std; // For time_t, time and ctime;
    time_t now = time(0);
    return ctime(&now);
}

void Network::TCPServer::handleIncomingConnection(boost::asio::ip::tcp::socket &socket)
{
    std::string remoteAddr = socket.remote_endpoint().address().to_string();
    Helpers::Logger::log() << "remote connetion: " << remoteAddr.c_str() << std::endl;

    std::string message = std::string("server time:") + getDateTime();

    boost::system::error_code ignored_error;
    boost::asio::write(socket, boost::asio::buffer(message), ignored_error);

    Helpers::Logger::log() << "message: " << message.c_str() << " sent to remote host" << std::endl;

    boost::asio::streambuf readBuffer;
    boost::asio::read_until(socket, readBuffer, '\n', ignored_error);

    std::istream is(&readBuffer);
    std::string incomingMessage;

    is >> incomingMessage;

    Helpers::Logger::log() << "message: " << incomingMessage.c_str() << " received from host" << std::endl;

    m_lastReceivedMessage = incomingMessage;

    notify();
}

void Network::TCPServer::run(const int port)
{
    Helpers::Logger::toLog("Start of TCP server...");

    using boost::asio::ip::tcp;

    try
    {
        boost::asio::io_service io_service;

        tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), port));

        while(true)
        {
            Helpers::Logger::log() << "wait for connection..." << std::endl;

            tcp::socket socket(io_service);
            acceptor.accept(socket);

            handleIncomingConnection(socket);
        }
    }
    catch (std::exception& e)
    {
        Helpers::Logger::log() << e.what() << std::endl;
    }
}
