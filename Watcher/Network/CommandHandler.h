#ifndef COMMANDHANDLER_H
#define COMMANDHANDLER_H

#include "ICommandHandler.h"
#include <vector>

namespace Network {

class CommandHandler : public ICommandHandler
{
public:
    CommandHandler();

public:
    // ITCPServerObserver interface
    virtual void messageReceived(const std::string& msg) override;

    // ICommandHandler interface
    virtual void attachObserver(ICommandHandlerObserver *observer) override;
    Command getLastCommand();

protected:
    virtual void notify() override;
    std::string parseCommandValue(const std::string& msg);
    std::string parseCommandStr(const std::string& msg);

private:
    std::vector<ICommandHandlerObserver*> m_observers;
    Command m_lastRecievedCommand;
};

}

#endif // COMMANDHANDLER_H
