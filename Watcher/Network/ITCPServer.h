#ifndef ITCPSERVER_H
#define ITCPSERVER_H

#include <string>
#include <vector>
#include "NetworkTypes.h"

namespace Network {

class ITCPServerObserver;

class ITCPServer
{
public:
    virtual ~ITCPServer(){}
    virtual void attachObserver(ITCPServerObserver* observer) = 0;
    virtual std::string getLastMessage() const = 0;
    virtual void run(const int port) = 0;

protected:
    virtual void notify() = 0;
};

class ITCPServerObserver
{
public:
    virtual ~ITCPServerObserver(){}
    virtual void messageReceived(const std::string& msg) = 0;
};

class BaseTCPServer : public ITCPServer
{
public:
    BaseTCPServer();

    // ITCPServer interface
public:
    void attachObserver(ITCPServerObserver *observer);
    std::string getLastMessage() const;

protected:
    void notify();

    std::vector<ITCPServerObserver *> m_observers;
    std::string m_lastReceivedMessage;

private:
    bool init();
    std::string getDateTime();
};

inline BaseTCPServer::BaseTCPServer()
{

}

inline void BaseTCPServer::attachObserver(ITCPServerObserver *observer)
{
    m_observers.push_back(observer);
}

inline std::string BaseTCPServer::getLastMessage() const
{
    return m_lastReceivedMessage;
}

inline void BaseTCPServer::notify()
{
    for(ITCPServerObserver* observer : m_observers)
    {
        observer->messageReceived(m_lastReceivedMessage);
    }
}

}

#endif // ITCPSERVER_H
