#ifndef NETWORKTYPES_H
#define NETWORKTYPES_H

#include <string>

namespace Network {

const char * const COMMAND_VALUE_SEPARATOR = ":";
const char * const INCORRECT_COMMAND_STR = "incorrent_command";

enum class eCommandTypes
{
    Empty = 0,
    TurnOnOff,
    TurnOnOffAll,
    Reboot,
    RebootAll,
    HardOff,
    HardOffAll
};

static std::string toString(const eCommandTypes commandType)
{
    std::string result;

    switch(commandType)
    {
    case eCommandTypes::Empty: result = "Empty"; break;
    case eCommandTypes::TurnOnOff: result = "TurnOnOff"; break;
    case eCommandTypes::TurnOnOffAll: result = "TurnOnOffAll"; break;
    case eCommandTypes::Reboot: result = "Reboot"; break;
    case eCommandTypes::RebootAll: result = "RebootAll"; break;
    case eCommandTypes::HardOff: result = "HardOff"; break;
    case eCommandTypes::HardOffAll: result = "HardOffAll"; break;
    default: result = INCORRECT_COMMAND_STR;
    }

    return result;
}

struct Command
{
    Command(eCommandTypes type, std::string value)
        : m_type(type)
        , m_value(value)
    {}

    inline eCommandTypes type() const  { return m_type; }
    inline const std::string& value() const { return m_value; }

private:
    eCommandTypes m_type;
    std::string m_value;
};

struct ServerInfo
{
    ServerInfo(const int port)
        : m_serverPort(port){}

    inline int serverPort() const { return m_serverPort; }

private:
    const int m_serverPort;
};

}

#endif // NETWORKTYPES_H
