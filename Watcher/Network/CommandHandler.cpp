#include "CommandHandler.h"
#include <string>
#include "boost/algorithm/string.hpp"
#include "Helpers/Logger.h"

Network::CommandHandler::CommandHandler()
    : m_lastRecievedCommand(Command(eCommandTypes::Empty, ""))
{

}

void Network::CommandHandler::messageReceived(const std::string& msg)
{
    Helpers::Logger::log() << "Network::CommandHandler::messageReceived:const std::string& msg:" << msg.c_str() << std::endl;

    if(boost::iequals(parseCommandStr(msg), "REBOOT"))
    {
        std::string value = parseCommandValue(msg);
        m_lastRecievedCommand = Command(eCommandTypes::Reboot, value);
    }
    else if(boost::iequals(parseCommandStr(msg), "TURNONOFF"))
    {
        std::string value = parseCommandValue(msg);
        m_lastRecievedCommand = Command(eCommandTypes::TurnOnOff, value);
    }
    else if(boost::iequals(parseCommandStr(msg), "HARDOFF"))
    {
        std::string value = parseCommandValue(msg);
        m_lastRecievedCommand = Command(eCommandTypes::HardOff, value);
    }
    else if(boost::iequals(parseCommandStr(msg), "REBOOTALL"))
    {
        m_lastRecievedCommand = Command(eCommandTypes::RebootAll, "");
    }
    else if(boost::iequals(parseCommandStr(msg), "TURNONOFFALL"))
    {
        m_lastRecievedCommand = Command(eCommandTypes::TurnOnOffAll, "");
    }
    else if(boost::iequals(parseCommandStr(msg), "HARDOFFALL"))
    {
        m_lastRecievedCommand = Command(eCommandTypes::HardOffAll, "");
    }
    else
    {
        m_lastRecievedCommand = Command(eCommandTypes::Empty, "");
    }



    if(m_lastRecievedCommand.type() != eCommandTypes::Empty)
    {
        notify();
    }
}

void Network::CommandHandler::attachObserver(Network::ICommandHandlerObserver *observer)
{
    m_observers.push_back(observer);
}

Network::Command Network::CommandHandler::getLastCommand()
{
    return m_lastRecievedCommand;
}

void Network::CommandHandler::notify()
{
    Helpers::Logger::log() << "Network::CommandHandler::notify() observers count: " << m_observers.size() << std::endl;
    Helpers::Logger::log() << "Network::CommandHandler::notify() command type: " << toString(m_lastRecievedCommand.type()) << std::endl
                           << "command value: " << m_lastRecievedCommand.value().c_str() << std::endl;
    if(!m_observers.empty())
    {
        for(ICommandHandlerObserver* observer : m_observers)
        {
            observer->commandReceived(m_lastRecievedCommand);
        }
    }
}

std::string Network::CommandHandler::parseCommandValue(const std::string &msg)
{
    std::size_t valueBegin = msg.find(COMMAND_VALUE_SEPARATOR) + 1;

    std::string commandValue(INCORRECT_COMMAND_STR);

    if(valueBegin > 0)
    {
        commandValue = msg.substr(valueBegin, msg.length());
    }

    return commandValue;
}

std::string Network::CommandHandler::parseCommandStr(const std::string &msg)
{
    std::size_t endOfCommand = msg.find(COMMAND_VALUE_SEPARATOR);

    std::string command(INCORRECT_COMMAND_STR);

    if(endOfCommand > 0)
    {
        command = msg.substr(0, endOfCommand);
    }

    return command;
}
