HEADERS += \
    $$PWD/CommandHandler.h \
    $$PWD/ICommandHandler.h \
    $$PWD/ITCPServer.h \
    $$PWD/NetworkTypes.h \
    $$PWD/TCPServer.h

SOURCES += \
    $$PWD/CommandHandler.cpp \
    $$PWD/TCPServer.cpp
