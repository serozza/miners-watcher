#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "ITCPServer.h"
#include <vector>
#include "boost/asio.hpp"


namespace Network {

class TCPServer : public BaseTCPServer
{
public:
    TCPServer(boost::asio::io_service& io_service);

    // ITCPServer interface
public:
    void run(const int port);

private:
    bool init();
    std::string getDateTime();
    void handleIncomingConnection(boost::asio::ip::tcp::socket& socket);
};

}

#endif // TCPSERVER_H
