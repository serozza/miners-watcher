#ifndef ICOMMANDHANDLER_H
#define ICOMMANDHANDLER_H

#include "ITCPServer.h"

namespace Network {

class ICommandHandlerObserver;

class ICommandHandler : public ITCPServerObserver
{
public:
    virtual ~ICommandHandler(){}
    virtual void attachObserver(ICommandHandlerObserver* observer) = 0;
    virtual Command getLastCommand() = 0;

protected:
    virtual void notify() = 0;
};

class ICommandHandlerObserver
{
public:
    virtual ~ICommandHandlerObserver(){}
    virtual void commandReceived(const Command command) = 0;
};

}

#endif // ICOMMANDHANDLER_H
